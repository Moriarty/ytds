package iamdev.me.ytds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import iamdev.me.ytds.entity.DocRecycle;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface DocRecycleMapper extends BaseMapper<DocRecycle> {

}
