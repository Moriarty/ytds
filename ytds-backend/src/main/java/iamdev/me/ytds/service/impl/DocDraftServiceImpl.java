package iamdev.me.ytds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import iamdev.me.ytds.entity.DocDraft;
import iamdev.me.ytds.mapper.DocDraftMapper;
import iamdev.me.ytds.service.IDocDraftService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class DocDraftServiceImpl extends ServiceImpl<DocDraftMapper, DocDraft> implements
    IDocDraftService {

}
